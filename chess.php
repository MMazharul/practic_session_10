<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chess board</title>
    <link rel="stylesheet" href="chessStyle.css">
</head>
<body onload="swing()">

<script>
    function swing()
    {
        document.getElementById('chessBoard').style.transform = "perspective(2000px) rotateX(50deg) rotateZ(1080deg)";
    }
</script>

<h1>input number between 2 - 15</h1>
<form method="post">
    <input type="number" name="number">
    <input type="submit" value="create chess board" name="submit">
</form>

<div id='chessBoard'>

    <?php
    if(isset($_POST['number'])) {
        $rowCol = $_POST['number'];
        if($rowCol>1 && $rowCol <=15) {
            for ($i = 1; $i <= $rowCol; $i++) {
                for ($j = 1; $j <= $rowCol; $j++) {
                    echo "<div class='hol'";
                    if ($i % 2 !== 0) {
                        if ($j % 2 !== 0) {
                            echo "style='background:#FFCC99'";
                        } else {
                            echo "style='background:#8F604F'";
                        }
                    } else {
                        if ($j % 2 !== 0) {
                            echo "style='background:#8F604F'";
                        } else {
                            echo "style='background:#FFCC99'";
                        }
                    }
                    echo "></div>";
                }
                echo "<br>";
            }
        }elseif($rowCol<2){
            echo "<br><hr>your value is very small";
        }else{
            echo "<br><hr>your value is so big";
        }
    }
    ?>

</div>
</body>
</html>